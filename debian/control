Source: nutsqlite
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-exec
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/nutsqlite
Vcs-Git: https://salsa.debian.org/med-team/nutsqlite.git
Homepage: http://nut.sourceforge.net/
Rules-Requires-Root: no

Package: nutsqlite
Architecture: all
Depends: ${misc:Depends},
         tk,
         libsqlite3-tcl,
         tcl-thread
Description: Dietary nutrition analysis software
 NUTsqlite uses the USDA database and stores this along with your personal data
 in a portable SQLite database allowing you to perform analysis and plan for
 your nutrition. Features include:
 .
  * The complete USDA database, your personal data, and the program code all
    stored in a portable SQLite database
  * Foods easy to find and add to daily meals
  * Configurable for 1-19 meals per day and any dietary plan--including
    ketogenic, low carb, zone, low fat
  * Comprehensive meal analysis for any number of consecutive meals
  * Presents both easy-to-read percentage summaries and in-depth nutrient
    analysis, including Omega-3 and Omega-6 essential fatty acids
  * Foods can be weighed in grams or ounces
  * Includes novel meal planning feature: you choose the food, NUT adjusts the
    quantities to your plan
  * Calorie Auto-Set feature uses linear regression on daily scale measurements
    of weight and body fat percentage to find optimal calorie level for
    improved body composition
  * Allows recording of recipes and customary meals for fast data entry
  * Sorts foods richest in each of the 150 nutrients
  * Reveals which foods contribute most to user's nutrition
