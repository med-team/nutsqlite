nutsqlite (2.0.6-4) unstable; urgency=medium

  * d/control: update uploader address
  * d/t/run-unit-test: update to newer `file` output.
    To ease the transition from file 5.44 to 5.45, the test suite is
    updated to recognize both outputs.
    Thanks to Christoph Biedl (Closes: #1051083)
  * d/control: declare compliance to standards version 4.6.2.
  * d/watch: update to version 4.

 -- Étienne Mollier <emollier@debian.org>  Sat, 02 Sep 2023 18:39:07 +0200

nutsqlite (2.0.6-3) unstable; urgency=medium

  * Added myself to Uploaders.
  * Added tcl-thread as dependency for nut.  (Closes: #973725)
  * Added superficial autopkgtest of update-nut.

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Wed, 04 Nov 2020 01:12:07 +0100

nutsqlite (2.0.6-2) unstable; urgency=medium

  * Replace myself instead of Iain R. Learmonth as Uploader since Ian
    has orphaned the package (thanks for your previous work Ian)
    Closes: #970279
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 15 Sep 2020 21:04:14 +0200

nutsqlite (2.0.6-1) unstable; urgency=medium

  * New upstream version

 -- Iain R. Learmonth <irl@debian.org>  Mon, 07 Jan 2019 11:25:19 +0000

nutsqlite (2.0.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #902994
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4

 -- Andreas Tille <tille@debian.org>  Wed, 04 Jul 2018 21:46:17 +0200

nutsqlite (1.9.9.6-1) unstable; urgency=medium

  * New upstream version 1.9.9.6
  * debian/control:
   - Update Standards-Version to 4.1.0
  * d/copyright:
   - Update format field to use HTTPS URL

 -- Iain R. Learmonth <irl@debian.org>  Thu, 31 Aug 2017 22:38:32 +0100

nutsqlite (1.9.9.3-2) unstable; urgency=medium

  * debian/copyright:
   - Addressing ftp-master comments
   - License is GPL-2+
   - Adding short GPL text

 -- Iain R. Learmonth <irl@debian.org>  Sun, 30 Oct 2016 22:41:24 +0000

nutsqlite (1.9.9.3-1) unstable; urgency=medium

  * Initial release. (Closes: #841178)

 -- Iain R. Learmonth <irl@debian.org>  Sat, 29 Oct 2016 20:10:01 +0100
